package string;

public class CutException extends RuntimeException
{
    CutException()
    {
        super();
    }

    protected CutException(final Object message)
    {
        super(message.toString());
    }
}
