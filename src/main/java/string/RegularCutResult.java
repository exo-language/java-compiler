package string;

final class RegularCutResult implements CutResult
{
    private final String remainder;
    private final String head;

    RegularCutResult(final String remainder, final String head)
    {
        this.remainder = remainder;
        this.head = head;
    }

    @Override
    public String getRemainder()
    {
        return this.remainder;
    }

    @Override
    public String getHead()
    {
        return this.head;
    }
}
