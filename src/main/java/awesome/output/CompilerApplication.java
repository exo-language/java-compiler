package awesome.output;

import awesome.expression.Expression;

import java.nio.file.Path;

final class CompilerApplication implements Application
{
    private final Expression expression;
    private final Path outputFile;

    CompilerApplication(final Expression expression, final Path outputFile)
    {
        this.expression = expression;
        this.outputFile = outputFile;
    }

    @Override
    public void run()
    {
        final var output = this.expression.reduce()
            .toLiteral();
        Application.fileOverwriteOf(this.outputFile, output)
            .run();
    }
}
