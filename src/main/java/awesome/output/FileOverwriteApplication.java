package awesome.output;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

final class FileOverwriteApplication implements Application
{
    private final Path fileToOverwrite;
    private final String newContent;

    FileOverwriteApplication(final Path fileToOverwrite, final String newContent)
    {
        this.fileToOverwrite = fileToOverwrite;
        this.newContent = newContent;
    }

    @Override
    public void run()
    {
        try
        {
            final var newContentBytes = this.newContent.getBytes(StandardCharsets.UTF_8);
            Files.write(this.fileToOverwrite, newContentBytes);
        }
        catch(final IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }
}
