package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.field.Field;
import string.CutException;

import java.nio.file.Path;
import java.util.Map;

public interface Parser<Remainder, Output>
{
    static Parser<String, Expression> referenceOf(final String input)
    {
        return new ReferenceParser(input);
    }

    static Parser<String, Expression> literalOf(final String input)
    {
        return new LiteralParser(input);
    }

    static Parser<String, Expression> objectOf(final String input)
    {
        return new ObjectParser(input);
    }

    static Parser<String, Expression> updateOf(final String input)
    {
        return new UpdateParser(input);
    }

    static Parser<String, Expression> accessOf(final String input)
    {
        return new AccessParser(input);
    }

    static Parser<String, Expression> expressionOf(final String input)
    {
        return new ExpressionParser(input);
    }

    static Parser<String, Expression> priorityOf(final String input)
    {
        return new PriorityParser(input);
    }

    static Parser<Void, Field> pathOf(final Path path)
    {
        return new PathParser(path);
    }

    default boolean canParse()
    {
        try
        {
            this.getRemainder();
            return true;
        }
        catch(final CutException e)
        {
            return false;
        }
    }

    Output parse();

    Remainder getRemainder();
}
