package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.ObjectExpression;
import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.intellij.lang.annotations.Language;
import string.StringCutter;

final class ObjectParser extends AbstractObjectParser
{
    @Language("RegExp")
    static final String startRegex = "[{]\\s*";
    @Language("RegExp")
    private static final String endRegex = "\\s*}";

    private static final StringCutter objectStartCutter = StringCutter.regex(startRegex);
    private static final StringCutter objectEndCutter = StringCutter.regex(endRegex);

    ObjectParser(final String initialInput)
    {
        super(initialInput);
    }

    @Override
    Expression parseFields(final ImmutableSet<Field> fields)
    {
        return ObjectExpression.ownerOf(fields);
    }

    @Override
    String getStartRemainder(final String remainder)
    {
        return ObjectParser.objectStartCutter.cutThenGetRemainder(remainder);
    }

    @Override
    String getEndRemainder(final String remainder)
    {
        return ObjectParser.objectEndCutter.cutThenGetRemainder(remainder);
    }
}
