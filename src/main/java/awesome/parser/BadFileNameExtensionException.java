package awesome.parser;

import string.CutException;

class BadFileNameExtensionException extends CutException
{
    BadFileNameExtensionException(final String fileName)
    {
        super("cannot parse %s because it has an invalid fileName");
    }
}
