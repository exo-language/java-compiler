package awesome.expression;

public abstract class TerminalExpression extends Expression
{
    @Override
    public abstract TerminalExpression copy();
    abstract TerminalExpression concat(TerminalExpression other);
    public abstract String toLiteral();
    public abstract ObjectExpression toObject();

}
