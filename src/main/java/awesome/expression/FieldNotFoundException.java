package awesome.expression;

class FieldNotFoundException extends RuntimeException
{
    FieldNotFoundException(final TerminalExpression expression, final String name)
    {
        super(String.format("could not find '%s' in '%s'", name, expression));
    }
}
