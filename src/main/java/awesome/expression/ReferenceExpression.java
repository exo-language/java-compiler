package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

public final class ReferenceExpression extends Expression
{
    public static Expression of(final String name)
    {
        return new ReferenceExpression(name);
    }

    private Field context;
    private final String name;

    private ReferenceExpression(final String name)
    {
        this.context = null;
        this.name = name;
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        if(this.context != null)
        {
            throw new ContextAlreadySetException();
        }
        this.context = parent;
    }

    @Override
    public Expression copy()
    {
        return new ReferenceExpression(this.name);
    }

    @Override
    public TerminalExpression reduce()
    {
        return this.context
            .find(this.name)
            .reduce()
            .copy();
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
