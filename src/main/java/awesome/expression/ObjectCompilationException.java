package awesome.expression;

class ObjectCompilationException extends RuntimeException
{
    ObjectCompilationException(final Expression expression)
    {
        super(String.format("cannot getOutput expression: %s", expression));
    }
}
