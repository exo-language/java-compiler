package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

public final class UpdateExpression extends Expression
{
    public static Expression of(
        final Expression sourceExpression,
        final Expression updateExpression)
    {
        return new UpdateExpression(sourceExpression, updateExpression);
    }

    private final Expression sourceExpression;
    private final Expression updateExpression;

    private UpdateExpression(
        final Expression sourceExpression,
        final Expression updateExpression)
    {
        this.sourceExpression = sourceExpression;
        this.updateExpression = updateExpression;
    }

    @Override
    public Expression copy()
    {
        final var newSourceExpression = this.sourceExpression.copy();
        final var updateExpression = this.updateExpression.copy();
        return new UpdateExpression(newSourceExpression, updateExpression);
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        this.sourceExpression.setContext(parent);
        this.updateExpression.setContext(parent);
    }

    @Override
    public TerminalExpression reduce()
    {
        final var sourceObject = this.sourceExpression.reduce().toObject();
        final var updateObject = this.updateExpression.reduce().toObject();
        return sourceObject.update(updateObject);
    }

    @Override
    public String toString()
    {
        final var rawFieldsString = this.updateExpression.toString();
        final var fieldsLength = rawFieldsString.length();
        final var fieldsString = rawFieldsString.substring(1, fieldsLength - 1);
        return String.format("%s[%s]", this.sourceExpression, fieldsString);
    }
}
