package awesome.expression.field;

import awesome.expression.Expression;
import awesome.expression.ObjectExpression;
import awesome.expression.TerminalExpression;

public final class ObjectField extends Field
{
    private final String name;
    private Expression expression;

    private ObjectExpression container;
    private Field context;

    ObjectField(final String name, final Expression expression)
    {
        this.name = name;
        this.expression = expression;

        this.container = null;
        this.context = null;
    }

    @Override
    public void setContainer(final ObjectExpression container)
    {
        this.container = container;
    }

    @Override
    public void setContext(final Field context)
    {
        this.context = context;
    }

    @Override
    public Field copy()
    {
        final var newField = Field.of(this.name, this.expression.copy());
        newField.setContext(this.context);
        return newField;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public TerminalExpression reduce()
    {
        return this.expression.reduce();
    }

    @Override
    public boolean canFind(final String name)
    {
        return this.containerContains(name)
            || this.contextCanFind(name);
    }

    @Override
    public Field find(final String name)
    {
        if(this.containerContains(name))
        {
            if(this.contextCanFind(name))
            {
                throw new DuplicatedReferenceException(name);
            }
            return this.container.get(name);
        }
        if(this.contextCanFind(name))
        {
            return this.context.find(name);
        }
        throw new ReferenceNotFoundException(name);
    }

    private boolean containerContains(final String name)
    {
        return (this.container != null) && this.container.contains(name);
    }

    private boolean contextCanFind(final String name)
    {
        return (this.context != null) && this.context.canFind(name);
    }

    @Override
    public String toString()
    {
        return String.format("%s: %s", this.name, this.expression.toString());
    }
}
