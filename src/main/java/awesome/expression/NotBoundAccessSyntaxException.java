package awesome.expression;

class NotBoundAccessSyntaxException extends RuntimeException
{
    NotBoundAccessSyntaxException(final Expression expression)
    {
        super(String.format("cannot find %s in nothing", expression));
    }
}
