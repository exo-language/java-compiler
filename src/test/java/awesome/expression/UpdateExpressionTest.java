package awesome.expression;

import awesome.expression.field.Field;
import awesome.parser.Parser;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class UpdateExpressionTest
{
    @Test
    void reduce_SHOULD_replaceOldFieldsWithNewFields()
    {
        final var expression = UpdateExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("k0", LiteralExpression.of("0")))),
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("k0", LiteralExpression.of("1")))));

        final var actualExpression = expression.reduce();

        final var actual = actualExpression
            .toObject().get("k0").reduce()
            .toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void reduce_SHOULD_preserveSourceObjectParent_WHEN_sourceIsAReference()
    {
        final var input = "{root: {k0: k1[f1: '1'] k1: {f0: f1 f1: k2} k2: '0'}}.root";
        final var expression = Parser.expressionOf(input).parse();

        final var actualExpression = expression.reduce();

        final var actual = actualExpression
            .toObject().get("k0").reduce()
            .toObject().get("f0").reduce()
            .toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void reduce_SHOULD_throw_WHEN_sourceDoesNotContainUpdatedField()
    {
        final var expression = UpdateExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("k0", LiteralExpression.of("0")))),
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("k1", LiteralExpression.of("1")))));

        ObjectExpression.ownerOf(
            ImmutableSet.of(
                Field.of("", expression)));

        assertThrows(IllegalUpdateException.class, expression::reduce);
    }
}
