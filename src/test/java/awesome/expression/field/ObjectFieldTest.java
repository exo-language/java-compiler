package awesome.expression.field;

import awesome.expression.LiteralExpression;
import awesome.expression.ObjectExpression;
import awesome.expression.TerminalExpression;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

final class ObjectFieldTest
{
    @Test
    void copy_SHOULD_createANewInstance()
    {
        final var field = Field.of("name", LiteralExpression.of(""));

        final var copy = field.copy();

        assertNotSame(field, copy);
    }

    @Test
    void copy_SHOULD_copyExpression()
    {
        final var mock = Mockito.mock(TerminalExpression.class);
        final var field = Field.of("name", mock);
        Mockito.when(mock.copy())
            .thenReturn(Mockito.mock(TerminalExpression.class));

        field.copy();

        Mockito.verify(mock, Mockito.times(1)).copy();
    }

    @Test
    void copy_SHOULD_resetContainer() throws Exception
    {
        final var field = Field.of("name", LiteralExpression.of(""));
        Field.literalOf(ObjectExpression.ownerOf(ImmutableSet.of(field)));
        final var contextClassField = ObjectField.class.getDeclaredField("container");
        contextClassField.setAccessible(true);

        final var copy = field.copy();
        assertNotNull(contextClassField.get(field));
        assertNull(contextClassField.get(copy));
    }

    @Test
    void copy_SHOULD_preserveContext() throws Exception
    {
        final var field = Field.of("name", LiteralExpression.of(""));
        field.setContext(Field.literalOf(LiteralExpression.of("")));
        final var contextClassField = ObjectField.class.getDeclaredField("context");
        contextClassField.setAccessible(true);

        final var copy = field.copy();
        assertNotNull(contextClassField.get(field));
        assertNotNull(contextClassField.get(copy));
    }
}
