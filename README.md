# Exo Compiler

**/!\\ Please see the [Scala version](https://gitlab.com/exo-language/scala-compiler) of this repository for a more reliable implementaiton /!\\**

## Features

* __Literal Expressions__:
    * [x] Literal Definition `'...'`
    * [x] Literal Escapes `'\\'` and `'\''`
    * [x] Literal Concatenation `'...'::'...'`
* __Object Expressions__:
    * [x] Object Definition `{...}`
    * [x] Key Definition `key: ...`
    * [x] Object Concatenation `{...}::{...}`
    * [x] Object Update `{...}::[...]`
    * [x] Object Access `{...}.field`
* __Reference Expressions__:
    * [x] Key Reference `thisIsARef`
    * [x] Path Reference `thisIsAlsoARef`

## Step By Step Build

Note: this project has been created with [Intellij IDEA](https://www.jetbrains.com/idea/).

* __Clone the repository__

    ```bash
    git clone https://gitlab.com/exo-language/java-compiler.git
    cd awlac-java
    ```

* __Complie__

    ```bash
    mvn clean compile
    ```

* __Run Tests__

    ```bash
    mvn test
    ```

* __Packaging__

    ```bash
    mvn assembly:single
    ```

    * This step creates the `target/awlac-java-1.0-jar-with-dependencies.jar` jar file
    * You can rename and move the file without breaking anything

* __One Liner__

    ```bash
    mvn clean compile test assembly:single
    ```

## Usage

__Use this line to compile Exo stuff__:

```bash
java -jar ${awlac-jar} "${value}" --context ${context} --output ${output}
```

- Please do not forget to replace all `${...}` parameters by their real world values
- The `${awlac-jar}` parameter stands for the jar file created by the __Packaging__ step above

Here are the command parameters:

- `value` : The Exo value to be evaluated (generally a reference to the main value, i.e. `context.main`)
- `-h`, `--help` : Prints a usage message similar to this one and exit.
- `-o`, `--output` : The output path of the Exo application (defaults to `awesome.out` in the working directory)
- `-c`, `--context` : The context path of the Exo application (defaults to the working directory itself)
    - `folders` are treated as Exo objects
    - `files` are treated as Exo values (files must end with `.al`, `.awla` or `.awesome`, otherwise they are ignored)

## Language Documentation

Have a look at the [Exo documentation](https://gitlab.com/exo-language/exo-standard/wikis/home) for more information about the Awesome Language.

## Built With

* [Java 11](https://docs.oracle.com/javase/8/docs/api/) - Main programming language
* [Maven](https://maven.apache.org/) - Dependency management and packaging

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) path for details.
